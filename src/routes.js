import React from 'react'

import { TouchableOpacity, AsyncStorage } from 'react-native'
import { createAppContainer, createSwitchNavigator } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { Feather, MaterialCommunityIcons, AntDesign } from '@expo/vector-icons'
import { createBottomTabNavigator } from 'react-navigation-tabs'

import Login from './pages/Login'
import Scan from './pages/Scan'
import Scanner from './pages/Scanner'
import HomeScreen from './pages/HomeScreen'
import HistoricScreen from './pages/HistoricScreen'
import Users from './pages/UsersListByStar'

console.disableYellowBox = true

const StackOne = createStackNavigator({
  Users: {
    screen: Users,
    navigationOptions: {
      title: 'Usuários com 5 estrelas',
      headerTitleStyle: {
        fontSize: 16,
      },
    },
  },
})

const StackThree = createStackNavigator({
  Historico: {
    screen: HistoricScreen,
    navigationOptions: {
      title: 'Fechar a aplicação',
      headerTitleStyle: {
        fontSize: 16,
      },
      headerShown: false,
    },
  },
})

const StackTwo = createAppContainer(
  createStackNavigator(
    {
      Coletar: {
        screen: Scan,
      },
      Scanner: {
        screen: Scanner,
      },
    },
    {
      defaultNavigationOptions: {
        headerTitleStyle: {
          fontSize: 16,
        },
      },
    },
  ),
)

const Modulo = createBottomTabNavigator(
  {
    Explore: {
      screen: HomeScreen,
      navigationOptions: {
        tabBarLabel: 'LISTA',
        tabBarIcon: ({ tintColor }) => (
          <Feather name="list" color={tintColor} size={20} />
        ),
      },
    },
    Coletar: {
      screen: StackTwo,
      navigationOptions: {
        tabBarLabel: 'COLETAR',
        tabBarIcon: ({ tintColor }) => (
          <MaterialCommunityIcons
            name="qrcode-scan"
            color={tintColor}
            size={20}
          />
        ),
      },
    },
    Users: {
      screen: StackOne,
      navigationOptions: {
        tabBarLabel: 'USUÁRIOS',
        tabBarIcon: ({ tintColor }) => (
          <Feather name="users" color={tintColor} size={20} />
        ),
      },
    },
    Historico: {
      screen: StackThree,
      navigationOptions: {
        tabBarLabel: 'MEUS DADOS',
        tabBarIcon: ({ tintColor }) => (
          <AntDesign name="profile" color={tintColor} size={20} />
        ),
      },
    },
  },
  {
    tabBarOptions: {
      activeTintColor: '#108F57',
      inactiveTintColor: '#c7c7c7',
      style: {
        backgroundColor: '#f5f5f5',
        borderTopWidth: 0,
        shadowOffset: { width: 3, height: 10 },
        shadowColor: 'black',
        shadowOpacity: 0.5,
        elevation: 5,
        height: 50,
      },
    },
  },
  {
    initialRouteName: 'Explore',
  },
)

const Routes = createAppContainer(
  createSwitchNavigator({
    Login,
    Modulo,
  }),
)

export default Routes
