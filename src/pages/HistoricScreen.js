import React, { useState, useEffect } from 'react'
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  AsyncStorage,
  TextInput,
  Image,
} from 'react-native'
import { Container, Content } from 'native-base'
import { AntDesign, FontAwesome5 } from '@expo/vector-icons'
import * as Font from 'expo-font'
import { AppLoading } from 'expo'
import { Block } from 'expo-ui-kit'
import logo from '../assets/logo-ecologica.png'
import api from '../services/api'

const fetchFonts = () => {
  return Font.loadAsync({
    'poppins-light': require('../fonts/Poppins-Light.ttf'),
  })
}

export default function HistoricScreen({ navigation }) {
  const [dataLoaded, setDataLoaded] = useState(false)
  const [usuario, setUsuario] = useState('')
  const [senha, setSenha] = useState('')
  const [icon, setItcon] = useState('black')
  const [passwordType, setPasswordType] = useState(true)

  if (!dataLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => setDataLoaded(true)}
      />
    )
  }

  function changePass() {
    passwordType == true ? setPasswordType(false) : setPasswordType(true)

    icon == 'black' ? setItcon('#8CB3AB') : setItcon('black')
  }

  async function handleLogout() {
    AsyncStorage.removeItem('token')
    navigation.navigate('Login')
  }

  async function handleEdit() {
    AsyncStorage.getItem('token').then(async token => {
      const response = await api.post('/admin/update', {
        token: token,
        usuario: usuario,
        senha: senha,
      })
      if (response.data.message == 'OK') {
        alert('Dados atualizados com sucesso')
      } else {
        alert(response.data.message)
      }
    })
  }

  return (
    <Block safe>
      <Container>
        <Content
          contentContainerStyle={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <Text style={{ fontFamily: 'poppins-light', marginBottom: 20 }}>
            Alterar meus dados
          </Text>
          <TextInput
            placeholder="Nome de usuário"
            autoCorrect={false}
            style={styles.input}
            value={usuario}
            onChangeText={setUsuario}
          />
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <TextInput
              placeholder="Senha"
              style={styles.input}
              placeholderTextColor="#B4B4B4"
              secureTextEntry={passwordType}
              autoCorrect={false}
              value={senha}
              onChangeText={setSenha}
            />
            <FontAwesome5
              name="eye"
              onPress={changePass}
              style={{
                color: icon,
                fontSize: 20,
                marginLeft: -45,
                marginTop: -25,
                padding: 10,
              }}
            />
          </View>
          <TouchableOpacity style={styles.btnSubmit} onPress={handleEdit}>
            <Text style={{ fontFamily: 'poppins-light', color: 'white' }}>
              Alterar
            </Text>
          </TouchableOpacity>

          <Text
            style={{
              fontFamily: 'poppins-light',
              margin: 20,
              marginTop: 20,
            }}
          >
            Ecologica powered by Mowah Tecnologia
          </Text>

          <TouchableOpacity
            style={{
              width: 120,
              height: 46,
              borderRadius: 8,
              textAlign: 'center',
              backgroundColor: '#c00',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={handleLogout}
          >
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <AntDesign
                name="logout"
                color="white"
                style={{
                  fontSize: 20,
                  marginLeft: -10,
                  marginTop: -5,
                  marginRight: 10,
                }}
              />
              <Text
                style={{
                  color: 'white',
                  fontSize: 16,
                  fontFamily: 'poppins-light',
                }}
              >
                Sair
              </Text>
            </View>
          </TouchableOpacity>
        </Content>
      </Container>
    </Block>
  )
}

const styles = StyleSheet.create({
  headerFilter: {
    flexDirection: 'row',
    margin: 5,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
  },
  textHeaderFilter: {
    fontFamily: 'poppins-light',
  },
  icon: {
    color: '#FFFF',
    marginTop: 20,
    fontSize: 22,
    marginLeft: -90,
  },
  input: {
    fontFamily: 'poppins-regular',
    paddingHorizontal: 20,
    backgroundColor: '#f2f2f2',
    fontSize: 14,
    height: 46,
    width: 280,
    marginBottom: 25,
    borderRadius: 12,
  },
  picker: {
    marginLeft: 40,
    width: 180,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    borderWidth: 3,
    borderTopColor: '#000',
    shadowOpacity: 0.25,
    shadowRadius: 6,
    elevation: 5,
    fontFamily: 'poppins-light',
  },
  listCollect: {
    margin: 20,
    flexDirection: 'row',
    marginTop: 4,
  },
  iconCollect: {
    backgroundColor: '#195F57',
    color: 'white',
    width: 50,
    height: 50,
    fontSize: 25,
    textAlign: 'center',
    lineHeight: 46,
    borderRadius: 100,
    marginTop: -4,
  },
  list1: {
    marginLeft: 10,
  },
  line1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 260,
  },
  listText: {
    fontFamily: 'poppins-light',
    fontSize: 13,
  },
  btnSubmit: {
    backgroundColor: '#223737',
    width: 280,
    height: 50,
    borderRadius: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
})
