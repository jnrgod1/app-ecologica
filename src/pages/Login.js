import React, { useState, useEffect } from 'react'
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  KeyboardAvoidingView,
  Alert,
  ActivityIndicator,
  AsyncStorage,
} from 'react-native'
import * as Font from 'expo-font'
import { AppLoading } from 'expo'
import logo from '../assets/logo-ecologica.png'
import { TextInput } from 'react-native-gesture-handler'
import api from '../services/api'
import { FontAwesome5 } from '@expo/vector-icons'
import NetInfo from '@react-native-community/netinfo'

const fetchFonts = () => {
  return Font.loadAsync({
    'poppins-light': require('../fonts/Poppins-Light.ttf'),
    'poppins-regular': require('../fonts/Poppins-Regular.ttf'),
  })
}

export default function Login({ navigation }) {
  const [dataLoaded, setDataLoaded] = useState(false)
  const [email, setEmail] = useState('')
  const [senha, setSenha] = useState('')
  const [passwordType, setPasswordType] = useState(true)
  const [icon, setItcon] = useState('black')
  const [textButton, setText] = useState('flex')
  const [animating, setAnimating] = useState(false)

  useEffect(() => {
    AsyncStorage.getItem('token').then(token => {
      if (token) {
        navigation.navigate('Modulo')
      }
    })

    setText('flex')
    setAnimating(false)
  }, [])

  function changePass() {
    passwordType == true ? setPasswordType(false) : setPasswordType(true)

    icon == 'black' ? setItcon('#8CB3AB') : setItcon('black')
  }

  async function handleSubmit() {
    setText('none')
    setAnimating(true)

    if (email.trim() == '' || senha.trim() == '') {
      Alert.alert(
        'Aviso',
        'Preencha todos os campos',
        [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
        { cancelable: false },
      )
      setAnimating(false)
      setText('flex')
    } else {
      NetInfo.fetch().then(async state => {
        if (state.isInternetReachable == false) {
          alert('Sem conexão com a internet, tente novamente :(')
          setAnimating(false)
          setText('flex')
        } else {
          const response = await api.post('/admin/login', {
            usuario: email.trim(),
            senha: senha.trim(),
          })

          if (response.data.auth == true) {
            await AsyncStorage.setItem('token', response.data.token)

            navigation.navigate('Modulo')
          } else {
            Alert.alert(
              'Aviso',
              response.data.message,
              [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
              { cancelable: false },
            )
            setAnimating(false)
            setText('flex')
          }
        }
      })
    }
  }

  if (!dataLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => setDataLoaded(true)}
      />
    )
  }

  return (
    <View style={styles.container}>
      <Image source={logo} style={styles.logo} />
      <KeyboardAvoidingView behavior="padding">
        <TextInput
          placeholder="Nome de usuário admin"
          style={styles.input}
          placeholderTextColor="#B4B4B4"
          autoCorrect={false}
          value={email}
          onChangeText={setEmail}
        />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <TextInput
            placeholder="Senha"
            style={styles.input}
            placeholderTextColor="#B4B4B4"
            secureTextEntry={passwordType}
            autoCorrect={false}
            value={senha}
            onChangeText={setSenha}
          />
          <FontAwesome5
            name="eye"
            onPress={changePass}
            style={{
              color: icon,
              fontSize: 20,
              marginLeft: -45,
              marginTop: -25,
              padding: 10,
            }}
          />
        </View>
        <TouchableOpacity onPress={handleSubmit} style={styles.btnSubmit}>
          <ActivityIndicator size="large" color="white" animating={animating} />
          <Text
            style={{
              display: textButton,
              fontFamily: 'poppins-regular',
              color: 'white',
              marginTop: -35,
            }}
          >
            Entrar
          </Text>
        </TouchableOpacity>
      </KeyboardAvoidingView>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 270,
    height: 210,
    marginTop: -50,
    marginBottom: 30,
  },
  input: {
    fontFamily: 'poppins-regular',
    paddingHorizontal: 20,
    backgroundColor: '#f2f2f2',
    fontSize: 14,
    height: 46,
    width: 280,
    marginBottom: 25,
    borderRadius: 12,
  },
  btnSubmit: {
    backgroundColor: '#223737',
    width: 280,
    height: 50,
    borderRadius: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
})
