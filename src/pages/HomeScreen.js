import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, Picker } from 'react-native'
import { Container, Content } from 'native-base'
import * as Font from 'expo-font'
import { AppLoading } from 'expo'
import { Block } from 'expo-ui-kit'
import socket from '../services/socket'
import ListSolicitation from '../components/ListSolicitation'
import NetInfo from '@react-native-community/netinfo'

const fetchFonts = () => {
  return Font.loadAsync({
    'poppins-light': require('../fonts/Poppins-Light.ttf'),
    'poppins-regular': require('../fonts/Poppins-Regular.ttf'),
  })
}

export default function HomeScreen({ navigation }) {
  const [dataLoaded, setDataLoaded] = useState(false)
  const [bairro, setBairro] = useState('Jauari I ')

  useEffect(() => {
    socket.on('new', function(val) {
      console.log('Socket passou aqui')
    })

    NetInfo.fetch().then(state => {
      if (state.isConnected == false) {
        alert('Sem conexão com a internet :(')
      }
    })
  }, [bairro])

  if (!dataLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => setDataLoaded(true)}
      />
    )
  }

  return (
    <Block safe>
      <Container>
        <Content
          contentContainerStyle={{
            flex: 1,
          }}
        >
          <View style={styles.headerFilter}>
            <Text style={styles.textHeaderFilter}>Filtrar por bairro</Text>
            <Picker
              style={styles.picker}
              selectedValue={bairro}
              onValueChange={itemValue => {
                setBairro(itemValue)
              }}
            >
              <Picker.Item label="Jauari I" value="Jauari I " />
              <Picker.Item label="Jauari II" value="Jauari II" />
              <Picker.Item label="Prainha" value="Prainha " />
              <Picker.Item label="São Francisco" value="São Francisco " />
              <Picker.Item label="Santo Antônio" value="Santo Antônio " />
              <Picker.Item label="São Cristóvão" value="São Cristóvão " />
              <Picker.Item label="Tiradentes" value="Tiradentes " />
              <Picker.Item label="Araújo Costa" value="Araújo Costa " />
              <Picker.Item label="Eduardo Braga 1" value="Eduardo Braga 1 " />
              <Picker.Item label="Eduardo Braga 2" value="Eduardo Braga 2 " />
              <Picker.Item label="Mutirão" value="Mutirão " />
              <Picker.Item label="Mamoud Amed" value="Mamoud Amed " />
              <Picker.Item label="Jardim Florestal" value="Jardim Florestal " />
              <Picker.Item label="Nogueira Júnior" value="Nogueira Júnior " />
              <Picker.Item label="Pedreiras" value="Pedreiras " />
              <Picker.Item label="Iraci" value="Iraci" />
              <Picker.Item label="Centro" value="Centro " />
              <Picker.Item label="Santa Luzia" value="Santa Luzia " />
              <Picker.Item label="São Jorge" value="São Jorge " />
              <Picker.Item label="Bairro da Paz" value="Bairro da Paz " />
              <Picker.Item label="Jardim Adriana" value="Jardim Adriana " />
              <Picker.Item label="Colonia" value="Colônia " />
            </Picker>
          </View>
          <ListSolicitation bairro={bairro} />
        </Content>
      </Container>
    </Block>
  )
}

const styles = StyleSheet.create({
  headerFilter: {
    flexDirection: 'row',
    marginTop: 50,
    margin: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
  },
  textHeaderFilter: {
    fontFamily: 'poppins-light',
  },
  icon: {
    color: '#FFFF',
    marginTop: 20,
    fontSize: 22,
    marginLeft: -90,
  },
  picker: {
    marginLeft: 40,
    width: 180,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    borderWidth: 3,
    borderTopColor: '#000',
    shadowOpacity: 0.25,
    shadowRadius: 6,
    elevation: 5,
    fontFamily: 'poppins-light',
  },
})
