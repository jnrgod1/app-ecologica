import React, { useState, useEffect } from 'react'
import {
  Text,
  View,
  StyleSheet,
  Button,
  AsyncStorage,
  Alert,
} from 'react-native'
import { BarCodeScanner } from 'expo-barcode-scanner'
import api from '../services/api'

export default function App({ navigation }) {
  const [hasPermission, setHasPermission] = useState(null)
  const [scanned, setScanned] = useState(false)
  const [msg, setMsg] = useState('')

  const material = navigation.state.params.listaCollect

  function handleBack() {
    navigation.navigate('Modulo')
    console.log('lasfdks')
  }

  useEffect(() => {
    ;(async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync()
      setHasPermission(status === 'granted')
    })()
  }, [])

  const handleBarCodeScanned = ({ type, data }) => {
    AsyncStorage.getItem('token').then(async token => {
      const response = await api.post('/solicitacao/admin/collect', {
        user_code: data,
        materiais: material.materiais,
        estrelas: material.estrelas,
        token: token,
      })
      setMsg(response.data.message)
      Alert.alert(
        'Solicitação',
        response.data.message,
        [
          {
            text: 'Tentar novamente',
            onPress: () => {},
            style: 'cancel',
          },
          {
            text: 'Ok',
            onPress: () => {
              navigation.goBack()
            },
          },
        ],
        { cancelable: false },
      )
    })
    setScanned(true)
  }

  if (hasPermission === null) {
    return <Text>Solicitando permissão a camera</Text>
  }
  if (hasPermission === false) {
    return <Text>Acesso não liberado a camera</Text>
  }

  return (
    <View
      style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end',
      }}
    >
      <BarCodeScanner
        onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
        style={StyleSheet.absoluteFillObject}
      />

      {scanned && (
        <Button
          title={'Toque para escanear novamente'}
          onPress={() => setScanned(false)}
        />
      )}
    </View>
  )
}
