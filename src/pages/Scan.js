import React, { useState, useEffect } from 'react'
import {
  View,
  Text,
  StyleSheet,
  CheckBox,
  TouchableOpacity,
  TextInput,
  ScrollView,
  Picker,
} from 'react-native'
import { Container, Content } from 'native-base'
import * as Font from 'expo-font'
import { AppLoading } from 'expo'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import { TextInputMask } from 'react-native-masked-text'

const fetchFonts = () => {
  return Font.loadAsync({
    'poppins-light': require('../fonts/Poppins-Light.ttf'),
    'poppins-regular': require('../fonts/Poppins-Regular.ttf'),
  })
}

var items = []
var assiduidade = false
var separacao = false
var limpeza = false
var entrega = false

export default function ScanScreen({ navigation }) {
  const [dataLoaded, setDataLoaded] = useState(false)
  const [peso, setPeso] = useState(0)
  const [material, setMaterial] = useState('1')
  const [checked, setChecked] = useState(false)
  const [checked1, setChecked1] = useState(false)
  const [checked2, setChecked2] = useState(false)
  const [checked3, setChecked3] = useState(false)

  useEffect(() => {
    setPeso(0)
  }, [])

  function handleCheck() {
    setChecked(!checked)
    assiduidade = !assiduidade
  }

  function handleCheck1() {
    setChecked1(!checked1)
    separacao = !separacao
  }

  function handleCheck2() {
    setChecked2(!checked2)
    limpeza = !limpeza
  }

  function handleCheck3() {
    setChecked3(!checked3)
    entrega = !entrega
  }

  if (!dataLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => setDataLoaded(true)}
      />
    )
  }

  function handleAdd() {
    items.push({
      tipo_material_id: material,
      peso: peso,
    })
    setPeso(0)
  }

  async function handleSubmit() {
    var estrelas = {
      assiduidade: assiduidade,
      separacao: separacao,
      limpeza: limpeza,
      entrega: entrega,
    }

    var lista = [{ materiais: items, estrelas }]

    for (var i = 0; i < lista.length; i++) {
      var listaCollect = lista[i]
    }

    navigation.navigate('Scanner', { listaCollect })
    items = []
    setChecked(false)
    setChecked1(false)
    setChecked2(false)
    setChecked3(false)
  }

  return (
    <Container>
      <ScrollView>
        <Content
          contentContainerStyle={{
            flex: 1,
            margin: 20,
            marginTop: 5,
          }}
        >
          <View>
            <Text style={styles.text}>Tipo de Material</Text>
            <Picker
              style={styles.input}
              // mode="dropdown"
              selectedValue={material}
              onValueChange={itemValue => {
                setMaterial(itemValue)
                console.log(material)
              }}
            >
              <Picker.Item style={styles.pickerText} label="Papel" value="1" />
              <Picker.Item
                style={styles.pickerText}
                label="Plástico"
                value="2"
              />
              <Picker.Item style={styles.pickerText} label="Metal" value="3" />
              <Picker.Item
                style={styles.pickerText}
                label="Misturado"
                value="4"
              />
            </Picker>
            <Text style={styles.text}>Peso (kg)</Text>
            <TextInputMask
              autoCorrect={false}
              keyboardType="number-pad"
              style={styles.input}
              type={'money'}
              options={{
                precision: 2,
                separator: '.',
                // delimiter: '.',
                unit: '',
                // suffixUnit: '',
              }}
              value={peso}
              onChangeText={itemValue => {
                setPeso(itemValue)
              }}
            />
          </View>

          <TouchableOpacity onPress={handleAdd} style={styles.btnSubmit}>
            <Text style={styles.textSubmit}>Adicionar a Lista</Text>
          </TouchableOpacity>

          <Text style={styles.textCheck}>Checklist</Text>
          <View style={styles.checkBox}>
            <CheckBox
              center
              title="Click Here to Remove This Item"
              iconRight
              iconType="material"
              checkedIcon="clear"
              uncheckedIcon="add"
              onChange={handleCheck}
              value={checked}
            />
            <Text style={{ fontFamily: 'poppins-light' }}>Assiduidade</Text>
          </View>

          <View style={styles.checkBox}>
            <CheckBox
              center
              title="Click Here to Remove This Item"
              iconRight
              iconType="material"
              checkedIcon="clear"
              uncheckedIcon="add"
              onChange={handleCheck1}
              value={checked1}
              checkedColor="red"
            />
            <Text style={{ fontFamily: 'poppins-light' }}>Separação</Text>
          </View>

          <View style={styles.checkBox}>
            <CheckBox
              center
              title="Click Here to Remove This Item"
              iconRight
              iconType="material"
              checkedIcon="clear"
              uncheckedIcon="add"
              onChange={handleCheck2}
              value={checked2}
            />
            <Text style={{ fontFamily: 'poppins-light' }}>Limpeza</Text>
          </View>

          <View style={styles.checkBox}>
            <CheckBox
              center
              title="Click Here to Remove This Item"
              iconRight
              iconType="material"
              checkedIcon="clear"
              uncheckedIcon="add"
              onChange={handleCheck3}
              value={checked3}
            />
            <Text style={{ fontFamily: 'poppins-light' }}>
              Entrega no ponto de coleta
            </Text>
          </View>

          <TouchableOpacity onPress={handleSubmit} style={styles.btnSubmit2}>
            <MaterialCommunityIcons
              name="qrcode"
              style={{
                color: 'white',
                fontSize: 30,
                marginRight: 10,
              }}
            />
            <Text style={styles.textSubmit}>Enviar por QrCode</Text>
          </TouchableOpacity>
        </Content>
      </ScrollView>
    </Container>
  )
}

const styles = StyleSheet.create({
  icon: {
    color: '#FFFF',
    marginTop: 20,
    fontSize: 22,
    marginLeft: -90,
  },
  input: {
    backgroundColor: '#EEEEEE',
    height: 53,
    paddingHorizontal: 20,
    fontFamily: 'poppins-light',
  },
  input1: {
    backgroundColor: '#EEEEEE',
    width: 320,
    height: 53,
    color: 'black',
    paddingHorizontal: 20,
    fontFamily: 'poppins-regular',
  },
  text: {
    marginTop: 14,
    marginBottom: 10,
    fontFamily: 'poppins-light',
  },
  textCheck: {
    fontFamily: 'poppins-light',
    marginTop: 30,
  },
  checkBox: {
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  btnSubmit: {
    marginTop: 30,
    backgroundColor: '#223737',
    height: 53,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnSubmit2: {
    marginTop: 20,
    marginBottom: 10,
    backgroundColor: '#223737',
    height: 53,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  btnSubmit3: {
    marginTop: 20,
    marginBottom: 30,
    width: 320,
    backgroundColor: '#223737',
    height: 53,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textSubmit: {
    color: 'white',
    fontFamily: 'poppins-light',
  },
  pickerText: {
    fontFamily: 'poppins-light',
  },
})
