import React, { Component, useState, useEffect } from 'react'
import {
  View,
  Text,
  StyleSheet,
  Picker,
  TouchableOpacity,
  ScrollView,
  AsyncStorage,
  FlatList,
} from 'react-native'
import { Icon, Button, Container, Header, Content, Left } from 'native-base'
import { FontAwesome5 } from '@expo/vector-icons'
import * as Font from 'expo-font'
import { AppLoading } from 'expo'
import api from '../services/api'

const fetchFonts = () => {
  return Font.loadAsync({
    'poppins-light': require('../fonts/Poppins-Light.ttf'),
    'poppins-regular': require('../fonts/Poppins-Regular.ttf'),
  })
}

export default function HomeScreen({ navigation }) {
  const [dataLoaded, setDataLoaded] = useState(false)
  const [lista, setLista] = useState([])
  const [mes, setMes] = useState('01')

  function handleAtt() {
    AsyncStorage.getItem('token').then(tokenUser => {
      async function loadSolicitation() {
        const response = await api.get('/admin/topusers', {
          params: { token: tokenUser, mes: '03' },
        })
        setLista(response.data)
        console.log(response.data)
      }
      loadSolicitation()
    })
  }

  useEffect(() => {
    handleAtt()
  }, [])

  if (!dataLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => setDataLoaded(true)}
      />
    )
  }

  return (
    <Container>
      <Content
        contentContainerStyle={{
          flex: 1,
        }}
      >
        <View style={styles.headerFilter}>
          <Text style={styles.textHeaderFilter}>Filtrar por mês</Text>
          <Picker
            style={styles.picker}
            selectedValue={mes}
            onValueChange={itemValue => {
              setMes(itemValue)
            }}
          >
            <Picker.Item label="Janeiro" value="01" />
            <Picker.Item label="Fevereiro" value="02" />
            <Picker.Item label="Março" value="03" />
            <Picker.Item label="Abril" value="04" />
            <Picker.Item label="Maio" value="05" />
            <Picker.Item label="Junho" value="06" />
            <Picker.Item label="Julho" value="07" />
            <Picker.Item label="Agosto" value="08" />
            <Picker.Item label="Setembro" value="09" />
            <Picker.Item label="Outubro" value="10" />
            <Picker.Item label="Novembro" value="11" />
            <Picker.Item label="Dezembro" value="12" />
          </Picker>
        </View>

        <FlatList
          data={lista}
          renderItem={({ item }) => (
            <View style={styles.listCollect}>
              <TouchableOpacity onPress={() => navigation.navigate('')}>
                <FontAwesome5 name="truck-moving" style={styles.iconCollect} />
              </TouchableOpacity>
              <View style={styles.list1}>
                <View style={styles.line1}>
                  <Text style={styles.listText}>{item.cliente['nome']}</Text>
                  <Text style={styles.listText1}>{item.mes}</Text>
                </View>
                <Text style={styles.listText}>{item.ultima_entrega}</Text>
              </View>
            </View>
          )}
          keyExtractor={item => item.cliente_id.toString()}
          horizontal={false}
          refreshing={false}
          onRefresh={handleAtt}
        />
      </Content>
    </Container>
  )
}

const styles = StyleSheet.create({
  headerCamp: {
    marginTop: 40,
    alignItems: 'center',
  },
  textHeader: {
    fontFamily: 'poppins-regular',
    fontSize: 15,
  },
  headerFilter: {
    flexDirection: 'row',
    marginTop: 10,
    margin: 20,
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 25,
  },
  textHeaderFilter: {
    fontFamily: 'poppins-light',
    fontSize: 15,
  },
  icon: {
    color: '#FFFF',
    marginTop: 20,
    fontSize: 22,
    marginLeft: -90,
  },
  picker: {
    marginLeft: 20,
    width: 150,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    borderWidth: 3,
    borderTopColor: '#000',
    shadowOpacity: 0.25,
    shadowRadius: 6,
    elevation: 5,
    fontFamily: 'poppins-light',
  },
  listCollect: {
    margin: 20,
    flexDirection: 'row',
    marginTop: 4,
  },
  iconCollect: {
    backgroundColor: '#195F57',
    color: 'white',
    width: 50,
    height: 50,
    fontSize: 20,
    textAlign: 'center',
    lineHeight: 46,
    borderRadius: 100,
    marginTop: -4,
  },
  list1: {
    marginLeft: 10,
  },
  line1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 260,
  },
  listText: {
    fontFamily: 'poppins-light',
    fontSize: 13,
  },
})
