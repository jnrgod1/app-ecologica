import axios from 'axios'

const api = axios.create({
  // baseURL: 'http://ecologica-mowahtecnologia-com.umbler.net',
  baseURL: 'http://162.241.105.239/ecologica/',
  // baseURL: 'http://10.0.0.163:3000',
})

export default api
