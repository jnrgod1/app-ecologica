import React, { useState, useEffect } from 'react'
import {
  View,
  FlatList,
  AsyncStorage,
  TouchableOpacity,
  StyleSheet,
  Text,
} from 'react-native'
import { FontAwesome5 } from '@expo/vector-icons'
import * as Font from 'expo-font'
import { AppLoading } from 'expo'

import api from '../services/api'

const fetchFonts = () => {
  return Font.loadAsync({
    'poppins-light': require('../fonts/Poppins-Light.ttf'),
    'poppins-regular': require('../fonts/Poppins-Regular.ttf'),
  })
}

export default function ListSolicitation({ bairro }) {
  const [dataLoaded, setDataLoaded] = useState(false)
  const [lista, setLista] = useState([])

  function handleAtt() {
    AsyncStorage.getItem('token').then(tokenUser => {
      async function loadSolicitation() {
        const response = await api.get('/solicitacao/admin/get', {
          params: { token: tokenUser, bairro: bairro },
        })
        setLista(response.data)
        console.log(response.data)
      }
      loadSolicitation()
    })
  }

  useEffect(() => {
    handleAtt()
  }, [bairro])

  if (!dataLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => setDataLoaded(true)}
      />
    )
  }

  return (
    <FlatList
      data={lista}
      renderItem={({ item }) => (
        <View style={styles.listCollect}>
          <TouchableOpacity>
            <FontAwesome5 name="truck-moving" style={styles.iconCollect} />
          </TouchableOpacity>
          <View style={styles.list1}>
            <View style={styles.line1}>
              <Text style={styles.listText}>{item.endereco}</Text>
              <Text style={styles.listText1}>A coletar</Text>
            </View>
            <Text style={styles.listText}>{item.cliente['nome']}</Text>
          </View>
        </View>
      )}
      keyExtractor={item => item.solicitacao_id.toString()}
      horizontal={false}
      refreshing={false}
      onRefresh={handleAtt}
    />
  )
}

const styles = StyleSheet.create({
  listCollect: {
    margin: 20,
    flexDirection: 'row',
    marginTop: 4,
  },
  iconCollect: {
    backgroundColor: '#195F57',
    color: 'white',
    width: 50,
    height: 50,
    fontSize: 20,
    textAlign: 'center',
    lineHeight: 46,
    borderRadius: 100,
    marginTop: -4,
  },
  list1: {
    marginLeft: 10,
  },
  line1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '75%',
  },
  listText: {
    fontFamily: 'poppins-light',
    fontSize: 13,
    width: 200,
  },
  listText1: {
    fontFamily: 'poppins-regular',
    fontWeight: 'bold',
    fontSize: 14,
    width: 200,
  },
})
